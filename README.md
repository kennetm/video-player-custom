# Fork of Boris's Unfinished Video Player

Removed jquery and amended the code to work with iphone-inline-video https://www.npmjs.com/package/iphone-inline-video
I wouldn't relay on this if I was you...

MPEG-DASH/HLS/Progressive streaming HTML5 video player.

### Example

```js
(function () {
  'use strict';

  var video = require('unfinished-video-player');

  var container = document.getElementById('container');

  video
    .create({
        currentTimeChangeHandler: function (val, old) { console.log(val, old) } //override currentTimeChangeHandler method
     })
     .init({
        controls:   false,                          //defaults to true
        watch:      false,                          //watch video properties, defaults to true
        autoplay:   true,                           //defaults to true
        fill:       'cover'                         //defaults to 'contain',
        attributes: ['webkit-playsinline', 'muted'] // needed for inline-iphone - 'single' attribute statements only
     })
     .render(container)                             //append video to a container
     .setSrc('/videos/sample.mov/')                 //mpdashify encoded video files
     .setPoster('/images/poster.jpg');              //can leave blank if no poster is needed

})();
```


