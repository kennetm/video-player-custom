'use strict';

(function () {

    var _ = require('underscore');
    // var $ = require('jquery');
    var MediaStreamFactory
    var View;
    var player;

    _.extend(window, require('./vendor/dashjs/dash.all'));
    require('./vendor/radiantmedialyzer/rml');

    MediaStreamFactory = function (el, params) {
        var presets = {
                dash: function (el) {

                    var context = new Dash.di.DashContext();
                    var player = new MediaPlayer(context);

                    player.startup();
                    player.attachView(el);
                    player.setAutoPlay(params.autoplay);
                    player.getDebug()
                        .setLogToBrowserConsole(false);

                    return {
                        type: 'dash',
                        setSrc: function (url) {
                            player.attachSource(url + 'manifest.mpd');
                        }
                    }
                },

                hls: function (el) {
                    return {
                        type: 'hls',
                        setSrc: function (url) {
                            el.setAttribute('src', url + 'manifest.m3u8')

                        }
                    }
                },

                progressive: function (el) {
                    return {
                        type: 'progressive',
                        setSrc: function (url) {
                            el.setAttribute('src', url + '1280x720-2000k.mp4');
                        }
                    }
                }
            },

            media_stream = (function () {
                var rml = new RadiantML(),
                    mse = rml.mse(),
                    mp4H264AAC = rml.mp4H264AAC('high40'),
                    m4aHEAACv2 = rml.m4aHEAACv2(),
                    hls = rml.hlsVideo(),
                    preset = presets.progressive;

                if (params.adaptive && hls) {
                    preset = presets.hls;
                } else if (params.adaptive && mse && mp4H264AAC && m4aHEAACv2) {
                    preset = presets.dash;
                }

                return preset;
            })();

        return media_stream(el);
    };

    View = function () {
        var addPoster = function () {
                var img = document.createElement('img');
                var el = document.createElement('div');
                el.style.position = 'absolute';
                el.style.top = '0';
                el.style.left = '0';
                el.style.right = '0';
                el.style.bottom = '0';
                el.style.backgroundSize = 'cover';
                el.style.backgroundPosition = '50%';
                el.style.backgroundRepeat = 'no-repeat';

                el.style.display = 'none';
                img.addEventListener('load', function () {
                    el.style.backgroundImage = 'url(' + img.src + ')';
                    el.style.display = 'block';
                }.bind(this))

                return {
                    element: el,

                    set: function (url) {
                        img.setAttribute('src', url);
                    },

                    remove: function () {
                        if (img.parentNode) {
                            img.parentNode.removeChild(img);
                        }
                        if (el.parentNode) {
                            el.parentNode.removeChild(el);
                        }
                    }
                }
            },

            addContainer = function () {
                var container = document.createElement('div');
                container.style.position = 'absolute';
                container.style.width = '100%';
                container.style.height = '100%';
                container.style.overflow = 'hidden';

                return container;

            },

            addVideo = function (params) {

                var video = document.createElement('video');
                video.id = 'video-custom';
                video.style.position = 'absolute';
                video.style.top = '50%';
                video.style.left = '50%';
                video.style.transform = 'translate(-50%,-50%)';
                video.style.transform = '-webkit-transform: translate(-50%, -50%);'; // may need to add to css for Safari
                video.style.width = '100%';
                video.style.height = '100%';
                if (params.controls) {
                    video.setAttribute('controls', '')
                }
                if (params.autoplay) {
                    video.setAttribute('autoplay', '')
                }
                if (params.attributes) {
                    for (var i = 0; i < params.attributes.length; i++) {
                        video.setAttribute(params.attributes[i], '')
                    }
                }

                return video;
            },

            poster,
            container,
            fill;

        return {

            set: function (params) {

                //add container
                container = addContainer();

                //add poster
                poster = addPoster();

                // bind 'one' functions this
                this.onVideoPlayOne = this.onVideoPlayOne.bind(this);
                this.onResizeOne = this.onResizeOne.bind(this);
                this.onRemovePosterOne = this.onRemovePosterOne.bind(this);
                this.resize = this.resize.bind(this);

                //add video
                this.video = addVideo(params);
                this.video.style.width = '0';
                this.video.addEventListener('loadedmetadata', this.onResizeOne)
                this.video.addEventListener('play', this.onRemovePosterOne);

                container.appendChild(this.video);
                container.appendChild(poster.element);
                container.addEventListener('click', this.onVideoPlayOne);

                //add resize handler
                fill = params.fill;
                window.addEventListener('resize', this.resize);
                this.resize();

            },

            onVideoPlayOne: function () {
                container.removeEventListener('click', this.onVideoPlayOne);
                this.video.play();
            },

            onResizeOne: function () {
                this.video.removeEventListener('loadedmetadata', this.onResizeOne);
                this.resize();
            },
            onRemovePosterOne: function () {
                this.video.removeEventListener('play', this.onRemovePosterOne);
                this.removePoster();
            },

            setPoster: function (url) {
                this.video.style.display = 'none';
                poster.set(url);
            },

            removePoster: function () {
                this.video.style.display = 'block';
                poster.remove();
            },

            render: function (el) {
                console.log('container - ', container)
                el.appendChild(container);
            },


            resize: function () {
                var c_w = container.offsetWidth;
                var c_h = container.offsetHeight;
                var c_ratio = c_w / c_h;
                var v_ratio = this.video.offsetWidth / this.video.offsetHeight || (16/9);
                var w;
                var h;

                console.log('this.video.videoWidth ', this.video.offsetWidth)


                if (fill === 'cover') {
                    if (c_ratio < v_ratio) {
                        h = c_h;
                        w = c_h * v_ratio;
                    } else {
                        w = c_w;
                        h = c_w / v_ratio;
                    }
                } else {
                    if (c_ratio > v_ratio) {
                        h = c_h;
                        w = c_h * v_ratio;
                    } else {
                        w = c_w;
                        h = c_w / v_ratio;
                    }
                }

                this.video.style.width = w + 'px';
                this.video.style.height = h + 'px';

            },

            remove: function () {

                window.removeEventListener('resize', this.resize);

                this.video.removeEventListener('loadedmetadata', this.onResizeOne);
                this.video.removeEventListener('play', this.onRemovePosterOne);
                if (this.video.parentNode) {
                    this.video.parentNode.removeChild(this.video);
                }

                poster.remove();

                container.removeEventListener('click', this.onVideoPlayOne);
                if (container.parentNode) {
                    container.parentNode.removeChild(container);
                }

            }
        }
    };

    player = {

        config: {
            watch: true,
            adaptive: true,
            controls: true,
            autoplay: true,
            fill: 'cover',
            attributes: []
        },

        events: [
            'loadstart',
            'progress',
            'suspend',
            'abort',
            'error',
            'emptied',
            'stalled',
            'loadedmetadata',
            'loadeddata',
            'canplay',
            'canplaythrough',
            'playing',
            'waiting',
            'seeking',
            'seeked',
            'ended',
            'durationchange',
            'timeupdate',
            'play',
            'pause',
            'ratechange',
            'resize',
            'volumechange'
        ],
        properties: [
            'error',
            'src',
            'currentSrc',
            'crossOrigin',
            'networkState',
            'preload',
            'buffered',
            'readyState',
            'seeking',
            'currentTime',
            'duration',
            'paused',
            'defaultPlaybackRate',
            'playbackRate',
            'played',
            'seekable',
            'ended',
            'autoplay',
            'loop',
            'mediaGroup',
            'controller',
            'controls',
            'volume',
            'muted',
            'defaultMuted',
            'audioTracks',
            'videoTracks',
            'textTracks',
            'width',
            'height',
            'videoWidth',
            'videoHeight',
            'poster'
        ],

        errorChangeHandler: function (val, old) {
        },
        srcChangeHandler: function (val, old) {
        },
        currentSrcChangeHandler: function (val, old) {
        },
        crossOriginChangeHandler: function (val, old) {
        },
        networkStateChangeHandler: function (val, old) {
        },
        preloadChangeHandler: function (val, old) {
        },
        bufferedChangeHandler: function (val, old) {
        },
        readyStateChangeHandler: function (val, old) {
        },
        seekingChangeHandler: function (val, old) {
        },
        currentTimeChangeHandler: function (val, old) {
        },
        durationChangeHandler: function (val, old) {
        },
        pausedChangeHandler: function (val, old) {
        },
        defaultPlaybackRateChangeHandler: function (val, old) {
        },
        playbackRateChangeHandler: function (val, old) {
        },
        playedChangeHandler: function (val, old) {
        },
        seekableChangeHandler: function (val, old) {
        },
        endedChangeHandler: function (val, old) {
        },
        autoplayChangeHandler: function (val, old) {
        },
        loopChangeHandler: function (val, old) {
        },
        mediaGroupChangeHandler: function (val, old) {
        },
        controllerChangeHandler: function (val, old) {
        },
        controlsChangeHandler: function (val, old) {
        },
        volumeChangeHandler: function (val, old) {
        },
        mutedChangeHandler: function (val, old) {
        },
        defaultMutedChangeHandler: function (val, old) {
        },
        audioTracksChangeHandler: function (val, old) {
        },
        videoTracksChangeHandler: function (val, old) {
        },
        textTracksChangeHandler: function (val, old) {
        },
        widthChangeHandler: function (val, old) {
        },
        heightChangeHandler: function (val, old) {
        },
        videoWidthChangeHandler: function (val, old) {
        },
        videoHeightChangeHandler: function (val, old) {
        },
        posterChangeHandler: function (val, old) {
        },
        loadstartEventHandler: function (event) {
        },
        progressEventHandler: function (event) {
        },
        suspendEventHandler: function (event) {
        },
        abortEventHandler: function (event) {
        },
        errorEventHandler: function (event) {
        },
        emptiedEventHandler: function (event) {
        },
        stalledEventHandler: function (event) {
        },
        loadedmetadataEventHandler: function (event) {
        },
        loadeddataEventHandler: function (event) {
        },
        canplayEventHandler: function (event) {
        },
        canplaythroughEventHandler: function (event) {
        },
        playingEventHandler: function (event) {
        },
        waitingEventHandler: function (event) {
        },
        seekingEventHandler: function (event) {
        },
        seekedEventHandler: function (event) {
        },
        endedEventHandler: function (event) {
        },
        durationchangeEventHandler: function (event) {
        },
        timeupdateEventHandler: function (event) {
        },
        playEventHandler: function (event) {
        },
        pauseEventHandler: function (event) {
        },
        ratechangeEventHandler: function (event) {
        },
        resizeEventHandler: function (event) {
        },
        volumechangeEventHandler: function (event) {
        },

        init: function (params) {
            _.extend(this.config, params);

            this.view = View();
            this.view.set(this.config);

            this.video = this.view.video;

            this.stream = MediaStreamFactory(this.view.video, this.config);
            this._addEventListeners();
            return this;
        },

        setSrc: function (url) {
            this.stream.setSrc(url);
            return this;
        },

        setPoster: function (url) {
            this.view.setPoster(url);
            return this;
        },

        render: function (el) {
            this.view.render(el);
            return this;
        },

        play: function() {
            this.view.video.play();
            return this;
        },

        destroy: function () {
            this._removeEventListeners();
            this.view.remove();
        },

        _removeEventListeners: function () {
            var self = this;
            _.each(self.events, function (event) {
                self.view.video.removeEventListener(event, self[event + 'EventHandler']);
                self.view.video.removeEventListener(event, self._inspectProperties);
            });
        },

        _addEventListeners: function () {
            var self = this,
                dbfn = _.debounce(self._inspectProperties.bind(self), 40);
            _.each(self.events, function (event) {
                self.view.video.addEventListener(event, self[event + 'EventHandler']);
                if (self.config.watch) {
                    self.view.video.addEventListener(event, dbfn);
                }
            });
        },

        _inspectProperties: function () {
            var self = this;
            self.state = self.state || {};
            _.each(self.properties, function (prop) {
                var old = self.state[prop],
                    val = self.view.video[prop];
                if (old !== val) {
                    self.state[prop] = val;
                    self[prop + 'ChangeHandler'].call(self, val, old);
                }
            });
        }
    };

    module.exports = {
        create: function (params) {
            var Player = function () {
            };
            Player.prototype = _.create(player, params || {});
            return new Player;
        }
    };
})();